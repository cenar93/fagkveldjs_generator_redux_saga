export const postTodo = (data) => {
    return fetch("https://jsonplaceholder.typicode.com/todos", {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(data)
    }).then(res => res.json())
}

export const getTodos = () => {
    return fetch("https://jsonplaceholder.typicode.com/todos", {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
    }).then(res => res.json())
}