import { combineReducers } from 'redux'
import {saga2Reducer} from "./sagas/sagaExample2"
import {saga3Reducer} from "./sagas/sagaExample3"

export default combineReducers({
    saga2: saga2Reducer,
    saga3: saga3Reducer
})