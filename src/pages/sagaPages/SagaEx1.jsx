import React from "react";
import { useDispatch } from 'react-redux';

export default function SagaEx1() {
    const dispatch = useDispatch();
    window.dispatch = dispatch;
    return (
        <div>
            <button onClick={() => dispatch({ type: "HELLO_WORLD" })}>
                Dispatch HELLO_WORLD
            </button>
            <button onClick={() => dispatch({ type: "HELLO" })}>
                Dispatch HELLO
            </button>
            <button onClick={() => dispatch({ type: "WORLD" })}>
                Dispatch WORLD
            </button>
        </div>
    )
}