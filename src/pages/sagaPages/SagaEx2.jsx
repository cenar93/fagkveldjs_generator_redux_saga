import React from "react";
import {useSelector, useDispatch} from 'react-redux'

export default function SagaEx1() {
    const value = useSelector(state => state.saga2.value)
    const dispatch = useDispatch();
    window.dispatch = dispatch;
    return (
        <div>
            <button onClick={() => dispatch({type: "HELLO_LATEST"})}>
                DISPATCH HELLO_LATEST
            </button>
            <button onClick={() => dispatch({type: "HELLO_EVERY"})}>
                DISPATCH HELLO_EVERY
            </button>
            <button onClick={() => dispatch({type: "HELLO_LEADING"})}>
                DISPATCH HELLO_LEADING
            </button>
            <button onClick={() => dispatch({type: "TAKE1"})}>
                DISPATCH TAKE1
            </button>
            <button onClick={() => dispatch({type: "TAKE2"})}>
                DISPATCH TAKE2
            </button>
            <div>{value}</div>
        </div>
    )
}