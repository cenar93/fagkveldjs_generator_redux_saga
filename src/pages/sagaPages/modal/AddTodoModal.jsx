import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import React, { useState } from 'react';
import { useDispatch } from 'react-redux';


const useStyles = makeStyles(theme => ({
    button: {
        margin: theme.spacing(1),
    },
    input: {
        display: 'none',
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: 200,
    },
}));

export function AddTodoModal({ open = false }) {
    const classes = useStyles();
    const [title, setTitle] = useState("")
    const dispatch = useDispatch();

    if (!open){
        return <></>
    }

    return (
        <Dialog onClose={() => dispatch({ type: "ADD_TODO_MODAL_CANCEL" })} open={true}>
            <TextField
                label="Innhold"
                multiline
                rowsMax="4"
                className={classes.textField}
                onChange={(event) => setTitle(event.target.value)}
            />
            <div>
                <Button onClick={() => dispatch({ type: "ADD_TODO_MODAL_SUCCESS", todoData: { title } })}
                    variant="contained" color="primary" className={classes.button}>
                    Ok
                </Button>
                <Button onClick={() => dispatch({ type: "ADD_TODO_MODAL_CANCEL" })} variant="contained"
                    className={classes.button}>
                    Avbryt
                </Button>
            </div>
        </Dialog>
    )

}