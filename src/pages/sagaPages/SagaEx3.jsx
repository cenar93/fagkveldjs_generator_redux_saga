import React from "react";
import {useSelector, useDispatch} from 'react-redux'
import {AddTodoModal} from "./modal/AddTodoModal";
import "./styles.css"
export default function SagaEx3() {
    const todoData = useSelector(state => state.saga3.todoData)
    const todoPageOpen = useSelector(state => state.saga3.todoPageOpen)
    const addTodoModalOpen = useSelector(state => state.saga3.addTodoModalOpen)
    const dispatch = useDispatch();
    if (!todoPageOpen) {
        return <button onClick={() => dispatch({type: "OPEN_TODO_PAGE"})}>Open todo</button>
    }
    return (
        <div>
            <AddTodoModal open={addTodoModalOpen}/>
            <div>Todo liste</div>
            {todoData.map(data => <div className={"todoListItem"} key={data.title}><div>{data.title}</div></div>)}
            <button onClick={() => dispatch({type: "ADD_TODO"})}>Legg til</button>
        </div>
    )
}