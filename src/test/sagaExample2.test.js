import { expectSaga } from 'redux-saga-test-plan';
import { saga2Reducer, sagaExample2 } from '../sagas/sagaExample2';

it('saga2Test', () => {

    expectSaga(sayHelloSaga)
        .withReducer(saga2Reducer)

        .dispatch({ type: 'HELLO_LATEST' })
        //Assert that put with type "UPDATE_VALUE" and value: "HELLO!" is eventually called
        .put({ type: "UPDATE_VALUE", value: "HELLO!" })

        .dispatch({ type: 'TAKE1' })
        //Assert that put with type "UPDATE_VALUE" and value: "TAKE1" is eventually called
        .put({ type: "UPDATE_VALUE", value: "TAKE1" })

        .dispatch({ type: 'TAKE2' })
        //Assert that put with type "UPDATE_VALUE" and value: "TAKE2" is eventually called
        .put({ type: "UPDATE_VALUE", value: "TAKE2" })

        .hasFinalState({
            value: "TAKE2"
        })
        .run()
});



