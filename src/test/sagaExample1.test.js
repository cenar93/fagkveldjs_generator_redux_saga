import { testSaga } from 'redux-saga-test-plan';
import { helloWorldSaga } from "../sagas/sagaExample1";

it('helloWorldUnitTest', () => {

    testSaga(helloWorldSaga)
        .next()
        //Asserts that saga yields take with HELLO as type
        .take('HELLO')
        .next()
        //Asserts that saga yields take with WORLD as type
        .take("WORLD")
        .next()
        .isDone()
});



