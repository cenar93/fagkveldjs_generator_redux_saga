import { expectSaga } from 'redux-saga-test-plan';
import { call } from 'redux-saga/effects';
import { getTodos, postTodo } from "../Api/jsonplaceholderService";
import sagaExample3, { saga3Reducer, todoPage } from '../sagas/sagaExample3';

const todoData = [{ title: "test" }, { title: "test" }];
const todoPostData = [{title: "newTodo"}];

it('sagaExample3IntegrationTest1', () => {
    return expectSaga(todoPage)
        .withReducer(saga3Reducer)
        .provide([
            [call(getTodos), todoData],
        ])
        .hasFinalState({
            todoData: todoData,
            todoPageOpen: true
        })
        .run()
});

it('sagaExample3IntegrationTest2', () => {
    return expectSaga(sagaExample3)
        .withReducer(saga3Reducer)
        .provide([
            [call(getTodos), todoData],
            [call(postTodo, todoPostData), todoPostData],
        ])
  
        .put({type: "FETCH_TODO_LIST_SUCCESS", todoData: todoData})
        .put({type: "ADD_TODO_MODAL_OPEN"})
        .put({type: "ADD_TODO_MODAL_CLOSE"})
        .put({type: "ADD_TODO_SUCCESS", todoData: todoPostData})

        .dispatch({type: "OPEN_TODO_PAGE"})
        .dispatch({type: "ADD_TODO"})
        .dispatch({type: "ADD_TODO_MODAL_SUCCESS", todoData: todoPostData})
        .hasFinalState({
            todoData: [...todoData, todoPostData],
            todoPageOpen: true,
            addTodoModalOpen: false
        })
        .run()
});