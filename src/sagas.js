import sagaExample2 from "./sagas/sagaExample2"
import sagaExample1 from "./sagas/sagaExample1"
import {all } from 'redux-saga/effects'
import sagaExample3 from "./sagas/sagaExample3";
import logger from "./sagas/loggerSaga";

export default function* rootSaga() {
    return yield all([
        // logger(),
        sagaExample1(),
        sagaExample2(),
        sagaExample3()
    ])
}



