import { all, call, put, race, take, takeLeading, delay, retry } from 'redux-saga/effects';
import { getTodos, postTodo } from "../Api/jsonplaceholderService";

const initialState = {
    todoPageOpen: false,
    todoData: [],
    addTodoModalOpen: false
}

export const saga3Reducer = (state = initialState, action) => {

    switch (action.type) {
        case "FETCH_TODO_LIST_SUCCESS":
            return {
                todoData: action.todoData,
                todoPageOpen: true
            }
        case "ADD_TODO_MODAL_OPEN":
            return {
                ...state,
                addTodoModalOpen: true
            }
        case "ADD_TODO_MODAL_CLOSE":
            return {
                ...state,
                addTodoModalOpen: false
            }
        case "ADD_TODO_SUCCESS": {
            return {
                ...state,
                todoData: [...state.todoData, action.todoData]
            }
        }
        default:
            return state;
    }
}

function* fetchTodoData() {
    try {
        const { todoData} = yield race({
            todoData: call(getTodos),
            timeout: delay(5000)
        })
        if (todoData) {
            yield put({ type: "FETCH_TODO_LIST_SUCCESS", todoData: todoData.slice(0, 5) })
        } else {
            yield put({ type: "FETCH_TODO_LIST_TIMEOUT_ERROR" })
        }
    } catch (e) {
        yield put({ type: "FETCH_TODO_LIST_FAILURE" })
    }
}

function* addTodo() {
    yield put({ type: "ADD_TODO_MODAL_OPEN" });

    const { addTodoSuccess, canceled } = yield race({
        addTodoSuccess: take('ADD_TODO_MODAL_SUCCESS'),
        canceled: take('ADD_TODO_MODAL_CANCEL')
    })

    yield put({ type: "ADD_TODO_MODAL_CLOSE" });

    if (canceled) {
        return;
    }

    try {
        //Retry is combination of call and delay effects
        const todoData = yield retry(3, 1000, postTodo, addTodoSuccess.todoData)
        yield put({ type: "ADD_TODO_SUCCESS", todoData })
    } catch (e) {
        yield put({ type: "ADD_TODO_FAILURE" })
    }

}

function* removeTodo() {
    console.log("remove")
}

export function* todoPage() {
    try {
        yield* fetchTodoData();
        yield all([
            yield takeLeading('ADD_TODO', addTodo),
            yield takeLeading('REMOVE_TODO', removeTodo),
        ])
    } catch (e) { }

}

export default function* sagaExample3() {
    yield takeLeading('OPEN_TODO_PAGE', todoPage)
}