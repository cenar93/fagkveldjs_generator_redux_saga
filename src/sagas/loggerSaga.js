import {all, takeEvery} from 'redux-saga/effects'

function* log(value) {
    console.log("DISPATCHED", value)
}

export default function* logger() {
    yield all([
        //* is a wildcard which takes all dispatched actions
        yield takeEvery('*', log),
    ])
}