import { all, put, take, takeEvery, takeLatest, takeLeading } from 'redux-saga/effects';

export const saga2Reducer = (state = {value: ""}, action) => {

    switch (action.type) {
        case "UPDATE_VALUE":{
            return {
                value: action.value
            }
        }
    }
    return state;
}

export function* sayHelloSaga() {
    console.log('Hello!')
    yield put({type: "UPDATE_VALUE", value: "HELLO!"})
    
    yield take('TAKE1')
    console.log("After TAKE1, doing some processing......")
    yield put({type: "UPDATE_VALUE", value: "TAKE1"})
    
    yield take('TAKE2')
    console.log("After TAKE2, doing some processing......")
    yield put({type: "UPDATE_VALUE", value: "TAKE2"})

}

export default function* sagaExample2() {
    yield all([
        yield takeLatest('HELLO_LATEST', sayHelloSaga),
        yield takeEvery('HELLO_EVERY', sayHelloSaga),
        yield takeLeading('HELLO_LEADING', sayHelloSaga)
    ])
}

