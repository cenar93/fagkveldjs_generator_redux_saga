import { take, takeEvery } from 'redux-saga/effects';

export function* helloWorldSaga() {
    console.log('Start of Saga')
    yield take('HELLO')
    console.log("After HELLO, doing some processing......")
    yield take('WORLD')
    console.log("After WORLD, doing some processing......")
}

export default function* sagaExample1() {
    yield takeEvery('HELLO_WORLD', helloWorldSaga)
}

