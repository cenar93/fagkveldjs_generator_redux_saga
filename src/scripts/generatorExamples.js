//############## ITERATOR EXAMPLE #############################
const array = ["Hello", "World!"];

const iterator = array[Symbol.iterator]();
iterator.next()
//Returns {value: "Hello", done: false}
iterator.next()
//Returns {value: "World", done: false}
iterator.next()
//Returns {value: undefined, done: true}

//############## GENERATOR EXAMPLE 1 #############################
function* helloWorld() {
    yield 'Hello';
    yield 'World!';
}

const generator = helloWorld();
// generator.next()

function helloWorld2() {
    return "Hello World!"
}

console.log("regular", helloWorld2())
//Returns "Hello World!"

const generator = helloWorld();
console.log("generator next 1", generator.next())
//Returns {value: "Hello", done: false}
console.log("generator next 2", generator.next())
//Returns {value: "World!", done: false}
console.log("generator next 3", generator.next())
//Returns {value: undefined, done: true}

//############## GENERATOR EXAMPLE 2 #############################
function* userCredentials() {
    const username = yield "Username"
    const password = yield "Password"
    return { username, password };
}

const generator = userCredentials();
console.log(generator.next().value)
//Prints Username
console.log(generator.next("chuck_norris").value)
//Prints Password
console.log(generator.next("hunter2").value)
//Prints {username: "chuck_norris", password: "hunter2"}

//############## GENERATOR EXAMPLE 3 #############################


function* callbackGenerator() {
    let i = 1;
    while (true) {
        yield fetch("https://jsonplaceholder.typicode.com/todos/" + i).then(res => res.json());
        if (i === 3) {
            return
        }
        i = i + 1
    }
}
const generator3 = callbackGenerator();

console.log(await generator3.next().value)
//Prints "`todo 1`"
console.log(await generator3.next().value)
//Prints "`todo 2`"
console.log(await generator3.next().value)
//Prints "`todo 3`"
console.log(generator3.next())
//Prints "{value: undefined, done: true}"

//############## GENERATOR EXAMPLE 4 #############################
function* generator2() {
    yield "Generator 2 Hello"
    yield "Generator 2 World"
}

function* generator1() {
    yield "Generator 1 Hello"
    yield* generator2();
    yield "Generator 1 World"
}
const generator4 = generator1();

console.log(generator4.next().value)
//Prints "Generator 1 Hello"
console.log(generator4.next().value)
//Prints "Generator 2 Hello"
console.log(generator4.next().value)
//Prints "Generator 2 World"
console.log(generator4.next().value)
//Prints "Generator 1 World"

//############## GENERATOR EXAMPLE 5 #############################
function* commentsGenerator(postId) {
    let id = (postId - 1) * 5 + 1
    while (true) {
        const userInput = yield fetch(`https://jsonplaceholder.typicode.com/posts/1/comments?postId=${postId}&id=${id}`)
            .then(res => res.json());
        if (userInput) {
            return userInput;
        }
        id += 1;
    }
}

function* postGenerator() {
    let postId = 1;
    while (true) {
        const userInput = yield* commentsGenerator(postId)
        if (userInput === "NEXT_POST") {
            postId += 1;
        } else if (userInput === "PREVIOUS_POST" && postId > 1) {
            postId -= 1;
        }
    }
}

const posts = postGenerator();
console.log((await posts.next().value)[0])
console.log((await posts.next().value)[0])
console.log("NEXT_POST")
console.log((await posts.next("NEXT_POST").value)[0])
console.log((await posts.next().value)[0])
console.log("PREVIOUS_POST")
console.log((await posts.next("PREVIOUS_POST").value)[0])


